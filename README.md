# solar charge controller -MPPT board testing
MPPT stands for Maximum Power Point Tracking, a technique to regulate the charge of your battery bank. The function of an MPPT charge controller is analogous to the transmission in a car. When the transmission is in the wrong gear, the wheels do not receive maximum power.
A solar MPPT charge controller supports battery charging when connected between the battery and the solar panel. The specifications for this controller include a current rating of 10 amps and a voltage rating of 12 volts. It is constructed from materials such as mild steel (MS) and plastic, and it is designed to be maintenance-free.
![MPPT_solar_charge_controller](https://gitlab.com/jithinraveendraniy25/solar-charge-controller-mppt/-/raw/master/Images/51Jyc7fCs2L._SL1200_-removebg-preview(1).png?ref_type=heads)


# List of Parameters recorded

- lower&upper cuttofff votage measurment(as per the load)
- current_measurment(as per the load))


# Prerequisites

- variable power supply
- multimeter
- ammeter 

## Getting started

- Make sure that you have a varied power supply.
- Connect the baord  as shown in the [wiring Diagram](https://gitlab.com/jithinraveendraniy25/solar-charge-controller-mppt/-/blob/master/MPPT/MPPT%20Solar%20charge%20Controller%20test-1.pdf?ref_type=heads)
- Measure the readings (voltage and current) by using a multimeter
- calculate load terminal power by using the equation 
 **p = V * I**


# License

This project is licensed under the MIT License - see the LICENSE.md file for details

